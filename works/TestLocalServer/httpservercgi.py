# coding: UTF-8
from http.server import HTTPServer, CGIHTTPRequestHandler

host = 'localhost'
port = 8000
httpd = HTTPServer((host, port), CGIHTTPRequestHandler)
print('serving at port', port)
httpd.serve_forever()
