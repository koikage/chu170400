%functionとしbashからファイル名を渡して実行できるようにしてあるので嫌なら下１行コメントアウト
function identificater(filename)

%functionとして動かしたくない場合下2行のコメントを解除
%clear
%filename = '.wav';
LPC_CODE = 13;

if ~ischar(filename)
    fprintf('input file name with string(''filename'')\n');
    return;
end
%modelデータ insectmodeldata.matのロード。データがない場合はない model_makerを先に実行してるはず。
load('insectmodeldata');

getlpc = wavtolpc(filename);
if isempty(getlpc)
    fprintf('change wav file\n');
    return;
end
identificatelpc = getlpc(2:LPC_CODE+1);
testlabel = 0;

%normalize
for x = 1:1:LPC_CODE
    identificatelpc(x) = identificatelpc(x) - correction(x).min;
    identificatelpc(x) = identificatelpc(x) / correction(x).max;
end

[predictlabel,accuracy,dec_value] = svmpredict(testlabel,identificatelpc,model);
fprintf(strcat(data(predictlabel).name),'\n');

exit