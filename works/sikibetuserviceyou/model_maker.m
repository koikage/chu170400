%指定ディレクトリのトレーニングデータ群からモデルを生成する。
%TRAINING_DATA_PATHで指定したディレクトリに教師データ群をまとめておく
%種別名_tradataの名のディレクトリを用意する。
%wavetolpc.m,selectLR.m,noisecut.m
%svmtrain,svmpredict関連ファイル
%を必要とするので同じディレクトリに入れておく
clear

%const
LPC_CODE = 13;
TRAINING_DATA_PATH='semimodel';
MODEL_SAVE_PATH='';
%*tradataという名前のディレクトリを用意 * の部分が種の名前になる
TRAINING_DATA_NAME='*tradata';

current_path = path;
path(current_path, pwd());


cd(TRAINING_DATA_PATH);
training_dir = dir(TRAINING_DATA_NAME);
kind_n = length(training_dir);
for kindind = 1:kind_n
    cd(training_dir(kindind).name);
    wavelist = dir('*.wav');
    data(kindind).num = length(wavelist);
    lpclist = zeros(data(kindind).num,LPC_CODE);
    get_num = data(kindind).num;
    lpcind = 1;
    for dataind = 1:data(kindind).num
        lpcdata = wavtolpc(wavelist(dataind).name);
        if isempty(lpcdata)
            get_num = get_num - 1;
            fprintf('%s is faild' , wavelist(dataind).name);
            continue;
        end
        lpclist(lpcind,:) = lpcdata(2:LPC_CODE+1);
        lpcind = lpcind + 1;
    end
    data(kindind).lpclist = lpclist(1:get_num,:);
    data(kindind).num = get_num;
    data(kindind).name = training_dir(kindind).name(1:length(training_dir(kindind).name)-8);
    cd('..')
end
cd('..')
lpcdataset = [];
labelset = [];
for kindind = 1:kind_n
    label = zeros(data(kindind).num,1);
    label = label + kindind;
    lpcdataset = [lpcdataset ; data(kindind).lpclist];
    labelset = [labelset ; label];
end

%入力データは0~1の形に成型する。このときの値は保存しておき、識別の際もデータに同様の処理をかける。
%(識別の際は0~1に収まらなくても良い)
for x = 1:1:LPC_CODE;
    correction(x).min = min(lpcdataset(:,x));
    lpcdataset(:,x) = lpcdataset(:,x) - correction(x).min;
    correction(x).max = max(lpcdataset(:,x));
    lpcdataset(:,x) = lpcdataset(:,x) / correction(x).max;
end

%普通ganma,costは使用するデータに合わせて再設定(クロスバリテーション)するもの
%影響がほぼないはずなので今回は固定しておく
ganma = 10^0.8;
training_cost = 10^3;
options = sprintf('-t 2 -g %f -c %f',ganma,training_cost);
model = svmtrain(labelset,lpcdataset,options);
save('insectmodeldata','model','data','correction')
path(current_path);