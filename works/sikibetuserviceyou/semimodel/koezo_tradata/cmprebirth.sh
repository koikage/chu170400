#!/bin/sh
if [ $# -ne 1 ] ; then
    echo "set file name head"
    exit 1
fi
num=1
for file in $1_*.wav $1_*.WAV; do
    mv $file tmpora$num.wav > /dev/null 2>&1
    num=`expr $num + 1`
    #echo "tmp_$file"
done

for file1 in *.wav *.WAV; do
        for file2 in *.wav *.WAV; do
            cmp $file1 $file2 > /dev/null 2>&1
            if [ $? -eq 0 -a $file1 != $file2 ] ; then
                echo equls $file2
                rm $file2
            fi
    done
done

num=1
for file in *.wav *.WAV; do
    mv $file $1_$num.wav > /dev/null 2>&1
    num=`expr $num + 1`
done