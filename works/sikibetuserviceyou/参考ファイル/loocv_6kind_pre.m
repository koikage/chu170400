%make2015_12_8 UTF-8
clear
dialogtitle = ['1st_class_file';'2nd_class_file';'3rd_class_file';'4th_class_file';'5th_class_file';'6th_class_file'];
datalength = 0;
lpc_c=13;
leng=0;
kind = 6
filenums = zeros(1,kind);
setnum = 20;

%datakaisyu
for x=1 : kind ;
    [fname,dpath,findex] = uigetfile('*.*',dialogtitle(x,:),'MultiSelect','on');
    if findex==0
        disp('no file');
    else
        [filenum,n] = size(char(fname));
        filenums(x) = filenum;
        dataperkind = zeros(lpc_c,setnum*filenum);
        lpcnum = zeros(filenum,1);
        lpcstartind = zeros(filenum,1);
        for ind = 1:filenum
            clear new_data load_fname
            if filenum == 1
                load_fname = fname;
            else
                load_fname = char(fname(1,ind));
            end
            s = sprintf('%s%s',dpath,load_fname);
            new_data = load(s);
            new_data = new_data';
            sizetmp = size(new_data);
            lpcnum(ind) = sizetmp(2);

            if(ind == 1)
                lpcstartind(ind) = 1;
            else
                lpcstartind(ind) = lpcstartind(ind-1) + lpcnum(ind-1);
            end
            %if(sizetmp(1) ~= lpc_c+1 || sizetmp(2) ~= setnum)
            %   'size error'
            %  quit

            dataperkind(1:lpc_c,lpcstartind(ind):lpcstartind(ind) + lpcnum(ind) - 1) = new_data(2:lpc_c+1,:);
        end
    end
    dataperkind(:,1:lpcstartind(filenums(x)) + lpcnum(filenums(x)) - 1)
    if (x == 1)
        datas = dataperkind(:,1:lpcstartind(filenums(x)) + lpcnum(filenums(x)) - 1);
        lpcnums = lpcnum;
        lpcstartinds = lpcstartind;
    else
        datas = [datas dataperkind(:,1:lpcstartind(filenums(x)) + lpcnum(filenums(x)) - 1) ];
        lpcnums = [lpcnums ; lpcnum];
        lpcstartinds = [lpcstartinds ; lpcstartind];
    end
end

%label seisei
sizetmp = size(datas);
labels = zeros(sizetmp(2),1);
startind = zeros(1,kind+1);
filestartind = zeros(1,kind+1);
for x=1 :kind
    indsum = 1;
    fileindsum = 1;
    if(x > 1)
        indsum = startind(x-1) + sum(lpcnums(filestartind(x-1):filestartind(x-1) + filenums(x-1) - 1));
        fileindsum = filestartind(x-1) + filenums(x-1);
    end
    startind(x) = indsum;
    filestartind(x) = fileindsum;
end
startind(kind+1) = length(labels) + 1;
filestartind(kind+1) = sum(filenums) + 1;

for x=1:kind;
    labels(startind(x):startind(x+1)-1) = x;
end

datas = datas';

testind = 1;
answer = zeros(1,sum(filenums));
for x=1:kind;
        answer(testind:testind + filenums(x) -1) = x;
        testind = testind + filenums(x);
end


for x=2:1:kind;
    lpcstartinds(filestartind(x):filestartind(x+1) - 1) = lpcstartinds(filestartind(x):filestartind(x+1)  - 1)  + startind(x) - 1;
end

for x=1:1:13;
    datas(:,x) = datas(:,x) - min(datas(:,x));
    datas(:,x) = datas(:,x) / max(datas(:,x));
end

costnum = 6;
ganmanum = 6;
startcost = 3;
startganma = 0;
costwidth = 2;
ganmawidth = 2;

accs = zeros(costnum,ganmanum);
for costcoef = 1:1:costnum
    for ganmacoef = 1:1:ganmanum
        accuracys = zeros(sum(filenums),1);
        prelabels = zeros(setnum,sum(filenums));
        predicts = zeros(1,sum(filenums));
        labelnums = zeros(sum(filenums),kind);
        for x=1:sum(filenums);
            tradata = [datas(1:lpcstartinds(x),:)  ;  datas(lpcstartinds(x) + lpcnums(x) :length(labels),:) ];
            tralabel = [labels(1:lpcstartinds(x),:) ; labels(lpcstartinds(x) + lpcnums(x) : length(labels), :)];
            tesdata = datas(lpcstartinds(x) : lpcstartinds(x)+lpcnums(x) -1, :);
            teslabel = labels(lpcstartinds(x) : lpcstartinds(x)+lpcnums(x) -1, :);
            cost = 10^(startcost+(costcoef-1) * costwidth / (costnum-1));
%            cost = 10^4.16;%pre_semi
%            cost = 10^4;
            ganma = 10^(startganma+(ganmacoef-1) * ganmawidth /(ganmanum-1)) ;
%            ganma = 10^(0.24);%pre_semi
%            ganma = 10^1.2;
            options = sprintf('-t 2 -g %f -c %f',ganma,cost);
            model = svmtrain(tralabel,tradata,options);%-g 10^-0.9 -c 10^3
            [predictlabel,accuracy,dec_value] = svmpredict(teslabel,tesdata,model);
            accuracys(x) = accuracy(1);
            tmp_prelabel = zeros(setnum,1);
            tmp_prelabel(1:length(predictlabel)) = predictlabel;
            prelabels(:,x) = tmp_prelabel;

            labelnum = zeros(1,kind);
            for y=1 : kind;
               tmp = find(predictlabel == y);
                labelnum(y) = length(tmp);
            end
            tmp = find(labelnum == max(labelnum));
            predicts(x) = tmp(1);
        end
        acc = 0;
        for y=1:1:sum(filenums)
            if (answer(y) == predicts(y))
                acc = acc + 1;
            end
        end
        acc = acc/sum(filenums)
        accs(costcoef ,ganmacoef) = acc;
    end
end