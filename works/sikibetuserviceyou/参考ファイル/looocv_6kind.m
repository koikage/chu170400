%make2015_12_8 UTF-8
clear
dialogtitle = ['1st_class_file';'2nd_class_file';'3rd_class_file';'4th_class_file';'5th_class_file';'6th_class_file'];
datalength = 0;
lpc_c=13;
leng=0;
kind = 6
filenums = zeros(1,kind);
setnum = 20;

for x=1 : kind ;
    [fname,dpath,findex] = uigetfile('*.*',dialogtitle(x,:),'MultiSelect','on');
    if findex==0
        disp('no file');
    else
        [filenum,n] = size(char(fname));
        filenums(x) = filenum;
        dataperkind = zeros(lpc_c,setnum*filenum);
        for ind = 1:filenum
            clear new_data load_fname
            if filenum == 1
                load_fname = fname;
            else
                load_fname = char(fname(1,ind));
            end
            s = sprintf('%s%s',dpath,load_fname);
            new_data = load(s);
            new_data = new_data';
            sizetmp = size(new_data);
            if(sizetmp(1) ~= lpc_c+1 || sizetmp(2) ~= setnum)
                'size error'
                quit
            end
            dataperkind(1:lpc_c,setnum*(ind-1)+1:setnum*ind) = new_data(2:lpc_c+1,:);
        end
    end
    if (x == 1)
        datas = dataperkind;
    else
        datas = [datas dataperkind];
    end
end

sizetmp = size(datas);
labels = zeros(sizetmp(2),1);
startind = zeros(1,kind);
for x=1 :kind
    indsum = 1;
    if(x > 1)
        indsum = startind(x-1);
        indsum = indsum + filenums(x-1) * setnum;
    end
    startind(x) = indsum;
end
startind(kind+1) = length(labels) + 1;

for x=1:kind;
    labels(startind(x):startind(x+1)-1) = x;
end

datas = datas';


answer = zeros(1,sum(filenums));
testind = 1;
for x=1:kind;
        answer(testind:testind + filenums(x) -1) = x;
        testind = testind + filenums(x);
end

for x=1:1:13;
    datas(:,x) = datas(:,x) - min(datas(:,x));
    datas(:,x) = datas(:,x) / max(datas(:,x));
end

accs = zeros(11,11);
for costcoef = 1:1:11
    for ganmacoef =1:1:11
        accuracys = zeros(sum(filenums),1);
        prelabels = zeros(setnum,sum(filenums));
        predicts = zeros(1,sum(filenums));
        labelnums = zeros(sum(filenums),kind);
        for x=1:sum(filenums);
            tradata = [datas(1:setnum*(x-1),:)  ;  datas(setnum*x+1 :length(labels),:) ];
            tralabel = [labels(1:setnum*(x-1),:) ; labels(setnum*x+1 : length(labels), :)];
            tesdata = datas(1+setnum*(x-1) : setnum*x, :);
            teslabel = labels(1 + setnum*(x-1) : setnum *x,:);
            cost = 10^(4+(costcoef-1) * 2 / 10);
            ganma = 10^(-0.7+(ganmacoef-1) * 0.5 /10) ;
            options = sprintf('-t 2 -g %f -c %f',ganma,cost);
            model = svmtrain(tralabel,tradata,options);%-g -0.2  -c 10^5
            [predictlabel,accuracy,dec_value] = svmpredict(teslabel,tesdata,model);
            accuracys(x) = accuracy(1);

            prelabels(:,x) = predictlabel;

            labelnum = zeros(1,kind);
            for y=1 : kind;
               tmp = find(predictlabel == y);
                labelnum(y) = length(tmp);
            end
            tmp = find(labelnum == max(labelnum));
            predicts(x) = tmp(1);
        end
        acc = 0;
        for y=1:1:sum(filenums)
            if (answer(y) == predicts(y))
                acc = acc + 1;
            end
        end
        acc = acc/sum(filenums)
        accs(costcoef ,ganmacoef) = acc;
    end
end