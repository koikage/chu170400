function [y,flag] = noisecut(y_base,fs)
    y_n = y_base;
    flag = 0;
    y = 0;
    serchrange = 1;%sec
    threshold_amp = 0.6;%nor amplitude
    add_range = 0.3;%yobunnni kiru
    cut_range = 0.8;%kiridasi zikann [sec]
    range_cut_tank = 0;%senntou karano kiridasihaba
    y_n = abs(y_n);
    flag1 = 1;
    flag2 = 0;
    while flag1 == 1
        if length(y_n) < cut_range*1.5*fs %1.5sec hituyou
            'can not'
            flag2 = 1;
            break;
        end
        y_n = y_n/max(abs(y_n)); %nor
        ymax=find(y_n==1);
        flag1=0;
        max_point = ymax(1);
        if max_point - serchrange / 2 * fs < 0
            serchy = y_n(max_point + serchrange /2 *fs:length(y_n));
        elseif max_point + serchrange / 2 * fs > length(y_n)
            serchy = y_n(1:max_point - serchrange / 2 * fs);
        else
            serchy = [y_n(1:max_point - serchrange / 2 * fs) ; y_n(max_point + serchrange / 2 * fs : length(y_n))];
        end
        tmp = find(serchy > threshold_amp); %sikiiti wo koeta atai
        %figure(2)
        %plot(serchy);
        length(tmp)/length(y_n)
        if length(tmp)/length(y_n) < 0.002 %wariai
            flag1 = 1;%mouissyuu
        end
        %for x=1:fs*serchrange:length(y_n)-fs*serchrange;
        %    y_tmp = y_n((1:fs*serchrange) + x);
        %    num = find(y_tmp>threshold_amp);
        %    if length(num) < threshold_num;
        %        flag1 = 1;
        %    end
        %end
        if flag1==1
            if ymax(1) < length(y_n)/2
                y_n = y_n(max_point + fs*add_range:length(y_n));
                range_cut_tank = range_cut_tank + max_point + fs*add_range;
            else
                y_n = y_n(1:max_point-fs*add_range);
            end
        end
        %figure(1);
        %plot(y_base);
        base_max_point = range_cut_tank + ymax(1);
    end
    if flag2 == 1
        flag = 1;
        return;
    end
    range_front = 0;
    range_middle = 0;
    range_back = 0;
    if  max_point - fs * cut_range > 0
        range_front = y_n(max_point-fs*cut_range:max_point);
    end
    if (max_point - fs * cut_range / 2 > 0) & (max_point + fs * cut_range / 2 < length(y_n) )
        range_middle = y_n(max_point-fs*cut_range/2:max_point+fs*cut_range/2);
    end
    if max_point + fs *cut_range < length(y_n)
        range_back = y_n(max_point:max_point+fs*cut_range);
    end
    if(range_back==0) & (range_middle==0) & (range_front==0)
        quit;
    end
    if (mean(range_middle) >= mean(range_back)) & (mean(range_middle) >= mean(range_front))
        y_cut = y_base(base_max_point-fs*cut_range/2:base_max_point+fs*cut_range/2);
    elseif mean(range_front) >= mean(range_back)
        y_cut = y_base(base_max_point-fs*cut_range:base_max_point);
    else
        y_cut = y_base(base_max_point:base_max_point+fs*cut_range);
    end
    base_max_point
    y_cut = y_cut/max(abs(y_cut));
    y = y_cut;
end