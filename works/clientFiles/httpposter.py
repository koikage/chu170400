# coding: UTF-8
# http通信による文字POST,ファイルPOSTを行うクラス
import random
import urllib.request
import os.path
import mimetypes
import sys
import ssl
from io import IOBase


class HTTPPoster(object):
    """docstring for HTTPPoster"""
    # variables
    # private static const
    _MULTIPART_HEADERS = {
        "Content-Type": 'multipart/form-data; boundary=',
        "Accept-Language": "ja",
        }
    # private
    # str _url
    # bytes _body
    # str _headers

    # urlと辞書型のデータを受け取って初期化
    # //TODO 型チェックしてエラー吐くようにしたい
    def __init__(self, url, datadict):
        super(HTTPPoster, self).__init__()
        self._url = url
        self._body, self._headers = self._httpencode(datadict)

    # boundary文字(--から始まるデータないに同じ列が存在しないようなランダムな区切り文字列)
    def _make_boundary(self):
        randomchars = [chr(random.choice(range(97, 123))) for x in range(20)]
        # "区切り文字".join(イテレータ)でイテレータ内の文字を間に区切り文字を挟んで一体化
        boundary = "----------%s" % "".join(randomchars)
        return boundary

    # 辞書型データをエンコーディング fileが含まれていればmultipart_encode
    def _httpencode(self, datadict):
        havefile = False
        for key, data in datadict.items():
            if isinstance(data, IOBase):
                havefile = True
        if havefile:
            return self._multipart_encode(datadict)
        else:
            return self._normal_encode(datadict)

    # fileがある場合multipart_encodeのヘッダとbodyを作成してエンコード
    def _multipart_encode(self, datadict):
        boundary = self._make_boundary()
        bodys = ["--" + boundary]
        for key, value in datadict.items():
            # 各要素のヘッダ準備
            bodyheader = 'Content-Disposition: form-data; name = "%s"' % key
            # ファイルの場合
            if isinstance(value, IOBase):
                name = value.name
                # 名前がbyte型なら一旦OSごとのエンコーディング方式デコードする
                if(isinstance(name, bytes)):
                    name = name.decode(sys.getfilesystemencoding)
                bodyheader += '; filename="%s"' % os.path.split(name)[-1]
                bodys.append(bodyheader)
                # 名前からmimeタイプを推測
                mimetype = mimetypes.guess_type(value.name)
                if mimetype:
                    content_type = mimetype[0]
                    bodyheader = "Content-Type: %s" % content_type
                    bodys.append(bodyheader)
                bodys.append("Content-Transfer-Encoding: binary")
            else:
                bodys.append(bodyheader)
            bodys.append("")
            # 空行後にデータ記述
            if isinstance(value, IOBase):
                bodys.append(value.read())
            elif isinstance(value, str):
                bodys.append(value.encode("utf-8"))
            else:
                bodys.append(value)
            # データ終端boundary
            bodys.append("--" + boundary)
        # 最終行には--を追加
        bodys[-1] += "--"
        body = "".encode("utf-8")
        # ContentLengthを自動設定してもらうためにひとまとめにする
        for onebody in bodys:
            if isinstance(onebody, str):
                body = body + onebody.encode("utf-8")
            else:
                body = body + onebody
            # http上で改行コードはCRLF(win上では\nだけでいいかもしれない)
            body = body + "\r\n".encode('utf-8')
        headers = self._MULTIPART_HEADERS.copy()
        headers["Content-Type"] = headers["Content-Type"] + boundary
        return body, headers

    # fileがなく文字列要素のみの場合urlencodeを利用してエンコード
    def _normal_encode(self, datadict):
        body = urllib.parse.urlencode(datadict)
        body = body.encode('ascii')
        return body, None

    # urlにデータをPOSTし、帰って来たレスポンスを返り値とする
    def post(self):
        if self._headers is not None:
            req = urllib.request.Request(self._url, self._body, self._headers)
        else:
            req = urllib.request.Request(self._url, self._body)
        context = ssl._create_unverified_context()
        with urllib.request.urlopen(req, context=context) as response:
            page = response.read()
            print(page)
            return page

    # url変更 //TODO 型チェックとエラー吐くべき
    def seturl(self, url):
        self._url = url

    # 送信data変更 再エンコ
    def setdata(self, datadict):
        self._body, self._headers = self._httpencode(datadict)

# 単体テスト
if __name__ == "__main__":
    url = "http://localhost:8000/cgi-bin/cgitest.py"
    filename = "out.wav"
    with open(filename, "rb") as filebinary:
        data = {"name": "testes", "file": filebinary}
        poster = HTTPPoster(url, data)
        poster.post()
    data = {"name": "testes2", "file":  "notfile this is str"}
    poster.setdata(data)
    poster.post()
