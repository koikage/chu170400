import wiringpi as pi
import os


# Hdc1000のオブジェクトクラス
# wiringpiのsetupはやらないので呼び出す側でやってください
class Hdc1000(object):
    """docstring for Hdc1000"""
    # HDC1000のI2Cアドレス
    _I2CADDR = 0x40
    # 各レジスタアドレス
    _TEMP_REG = 0x00
    _HUMI_REG = 0x01
    _CONF_REG = 0x02

    # atOnce=Trueで温湿度同時取得
    def __init__(self, pin_RDY, atOnce=False):
        super(Hdc1000, self).__init__()
        self.i2c = pi.I2C()
        # I2C初期設定 戻り値はLinuxファイルハンドル
        self.dev = self.i2c.setup(self._I2CADDR)
        self.rdy = pin_RDY
        self.atOnce = atOnce
        # rdyピンをINPUTに
        pi.pinMode(self.rdy, pi.INPUT)
        if atOnce:
            # 0x0010はノーマル動作、同時取り込み、14ビット分解能、電源>2.8V
            # wiringpiではバイト順が反転する
            self.i2c.writeReg16(self.dev, self._CONF_REG, 0x0010)
        else:
            # 0x0000は取り込みがバラ
            self.i2c.writeReg16(self.dev, self._CONF_REG, 0x0010)

    def getTempreture(self):
        if self.atOnce:
            return self.getTempAndHumid()[0]
        else:
            self.i2c.writeReg16(self.dev, self._TEMP_REG, 0x0000)
            # 変換待機
            while pi.digitalRead(self.rdy) == pi.HIGH:
                pass
            # なぜか結果はストリームでとるらしい
            data = os.read(self.dev, 2)
            temp = (int.from_bytes(data, 'big') / (2.0**16)*165 - 40)
            return temp

    def getHumidity(self):
        if self.atOnce:
            return selg.getTempAndHumid()[1]
        else:
            self.i2c.writeReg16(self.dev, self._HUMI_REG, 0x0000)
            # 変換待機
            while pi.digitalRead(self.rdy) == pi.HIGH:
                pass
            # なぜか結果はストリームでとるらしい
            data = os.read(self.dev, 2)
            humi = (int.from_bytes(data, 'big') / (2.0**16)*100)
            return humi

    def getTempAndHumid(self):
        if self.atOnce:
            self.i2c.writeReg16(self.dev, self._TEMP_REG, 0x0000)
            # 変換待機
            while pi.digitalRead(self.rdy) == pi.HIGH:
                pass
            # なぜか結果はストリームでとるらしい
            data = os.read(self.dev, 4)
            temp = (int.from_bytes(data[0:2], 'big') / (2.0**16)*165 - 40)
            humi = (int.from_bytes(data[2:4], 'big') / (2.0**16)*100)
            return (temp, humi)
        else:
            return (self.getTempreture(), self.getHumidity())


if __name__ == "__main__":
    # wirigPi初期設定
    pi.wiringPiSetup()
    hdc1000 = Hdc1000(7, True)
    tes1 = hdc1000.getTempAndHumid()
    hdc1000 = Hdc1000(7)
    tes2 = hdc1000.getTempreture()
    tes3 = hdc1000.getHumidity()
    print(tes1)
    print(tes2)
    print(tes3)
