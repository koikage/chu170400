# coding: UTF-8
# クライアントmainファイル
# idの送信 録音 音量確認 周波数確認 音声痩身
from httpposterwithrequests import HTTPPosterWithRequests
from soundcontroller import SoundController
from hdc1000 import Hdc1000
import datetime
import wiringpi as pi


class Chu_Client(object):
    """docstring for Chu-Client"""
    # variables
    # private static const
    _CGIURL = "http://192.168.0.174/cgi-bin/fixedchucgi.py"
    # _CGIURL = "http://localhost:8000/cgi-bin/fixedchucgi.py"
    _ID = "1"
    _CERTFILE = ('client/certs/fixed-chu1.crt',
                 'client/private/fixed-chu1.key')
    # Hdc1000のRDYピンの場所
    _RDY_PIN = 7
    # private

    def __init__(self):
        super(Chu_Client, self).__init__()
        self._record = True
        self._verify = False
        self._cert = True

    def main(self):
        pi.wiringPiSetup()
        while self._record:
            (status, filename) = self._getsoundfile()
            if status:
                env_data = self._getenvdata()
                self._sendonedaydata(filename, env_data)
                # self._senddata()

    # 録音を行い、識別に進める条件を満たすかを返す
    def _getsoundfile(self):
        sc = SoundController()
        status, filename = sc.getsound(self._ID)
        print("finish recording status %s " % status)
        return (status, filename)

    # 音声データ、環境データをまとめてPOSTする
    def _senddata(self, filename, env_data):
        print("posting start")
        # //TODO 環境情報
        with open(filename, "rb") as wavefile:
            d = datetime.datetime.today()
            dstr = d.strftime("%Y%m%d%H%M%S")[2:]
            data = {'name': 'insect_wave', 'id': self._ID, 'datetime1': dstr,
                    'num': 1, 'file1': wavefile,
                    'temp1': env_data[0], 'humid1': env_data[1]}
            poster = self._posterinit(data)
            poster.post()
            print("posting end")

    # 1日分のデータを送る
    def _sendonedaydata(self, filename, env_data):
        # まだ１つのデータを送るだけの仮実装
        self._senddata(filename, env_data)

    def _posterinit(self, data):
        poster = HTTPPosterWithRequests(
            self._CGIURL, data, self._verify, self._cert)
        return poster

    # 環境センサの情報を取得する (温度、湿度)のタプルを返す
    def _getenvdata(self):
        temp_humi_dev = Hdc1000(self._RDY_PIN, True)
        return temp_humi_dev.getTempAndHumid()


if __name__ == '__main__':
    chu_client = Chu_Client()
    chu_client.main()
