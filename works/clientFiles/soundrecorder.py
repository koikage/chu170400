# coding: UTF-8
import pyaudio
import wave
from datetime import datetime


class SoundRecorder(object):
    """docstring for SoundRecorder"""
    # variables
    # private static const
    _FORMAT = pyaudio.paInt16
    _CHANNELS = 1
    _RATE = 48000
    _TIME_S = 10
    _DEVIE_INDEX = 0
    _CHUNK = 2**11
    # private static
    # public static
    # private
    # public
    # int _samplesize
    # byte[] _frames

    def __init__(self):
        super(SoundRecorder, self).__init__()

    def record(self):
        audio = pyaudio.PyAudio()
        stream = audio.open(
            format=self._FORMAT,
            channels=self._CHANNELS,
            rate=self._RATE,
            input=True,
            input_device_index=self._DEVIE_INDEX,
            frames_per_buffer=self._CHUNK)
        print("recoding now")
        frames = []
        for i in range(0, int(self._RATE / self._CHUNK * self._TIME_S)):
            data = stream.read(self._CHUNK)
            frames.append(data)
        print("finish")
        stream.stop_stream()
        stream.close()
        audio.terminate()
        self._frames = frames
        self._samplesize = audio.get_sample_size(self._FORMAT)
        # フレームをまとめてバイナリで返すか、ファイルセーブして後から読ませるか

    # 録音時刻をファイル目に明記してファイル保存
    # headに入れた文字列はファイル名の最小に入る
    def save(self, head=''):
        day = datetime.today()
        day = day.timetuple()
        filename = ""
        for i in range(0, 6):
            filename += ("%d" % day[i] + " ")[-3:-1]
        filename = "%s_%s.wav" % (head, filename)
        wavefile = wave.open(filename, "wb")
        wavefile.setnchannels(self._CHANNELS)
        wavefile.setsampwidth(self._samplesize)
        wavefile.setframerate(self._RATE)
        wavefile.writeframes(b"".join(self._frames))
        wavefile.close()
        return filename

if __name__ == '__main__':
    recorder = SoundRecorder()
    recorder.record()
    recorder.save()
