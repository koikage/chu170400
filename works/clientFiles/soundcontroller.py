# coding: UTF-8
from soundrecorder import SoundRecorder
from soundprocessor import SoundProcessor


# fixed-chu用の録音制御用クラス、録音から処理、判定間で行う。
class SoundController(object):
    """docstring for SoundController"""
    # variables
    # private static const
    # private static
    # public static
    # private
    # SoundRecorder _recorder
    # SoundProcesser _processer
    # public

    def __init__(self):
        super(SoundController, self).__init__()
        self._recorder = SoundRecorder()
        self._processer = SoundProcessor()

    def getsound(self, id):
        wave = None
        is_got = False
        self._recorder.record()
        wave = self._recorder.save("ID%s" % id)
        is_got = self._processer.checkwave(wave)
        if not is_got:
            self._processer.deleatewave(wave)
        return(is_got, wave)

if __name__ == '__main__':
    sc = SoundController()
    print(sc.getsound(3))
