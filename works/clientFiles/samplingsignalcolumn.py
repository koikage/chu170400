# coding: UTF-8
import numpy as np
from scipy import fftpack


# サンプリングされた信号列の処理用
# intervalを入れておくとスペクトラムを返せる
class SamplingSignalColumn(object):

    # 信号を引数に初期化、オプションでサンプリング間隔を指定、デフォルトは1
    def __init__(self, waveform, interval=None):
        super(SamplingSignalColumn, self).__init__()
        self._waveform = waveform
        self._length = len(waveform)
        if interval is None:
            self._interval = 1
        else:
            self._interval = interval

    def rms(self):
        waveform = self._waveform
        return np.sqrt(np.mean(waveform * waveform))

    def ampspectrum(self):
        waveform = self._waveform
        datalen = len(waveform)
        fftwave = fftpack.fft(waveform)
        freq = fftpack.fftfreq(datalen, self._interval)
        halflen = int(datalen/2)
        fftwave = np.abs(fftwave[1:halflen])
        freq = freq[1:halflen]
        return (fftwave, freq)

    def powerspectrum(self):
        fftwave, freq = self.ampspectrum()
        fftwave = fftwave * fftwave
        return (fftwave, freq)
