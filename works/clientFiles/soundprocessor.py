# coding: UTF-8
import scipy.io.wavfile as wavfile
import numpy as np
import os
from samplingsignalcolumn import SamplingSignalColumn

# cgiで使うの禁止
# import matplotlib.pyplot as plt


class SoundProcessor(object):
    """docstring for SoundProcesser"""
    # instance variable
    # private static const
    # 閾値音圧[dB]
    _DB_THRESHOLD = 20
    _RMS_THRESHOLD = 0
    # 使用するマイクの感度[dB]
    _DEVICE_SENS = -32
    # コンピュータによるマイク増幅[dB]
    _AMP_GAIN = 0
    # 基準音圧(Pa)
    _STANDARD_PA = 20 * (0.1**6)
    # 基準周波数上下
    _FREQ_UPPER = 20000
    _FREQ_LOWER = 0
    # private

    def __init__(self):
        super(SoundProcessor, self).__init__()
        # TODO ファイルから読み込むようにする
        # self._db_threshold =

    def checkwave(self, filename):
        sampling_rate, waveform = wavfile.read(filename)
        ssc = SamplingSignalColumn(waveform, 1/sampling_rate)
        return self._checkvolume(ssc) and self._checkfrequency(ssc)

    def deleatewave(self, filename):
        print("remove")
        os.remove(filename)

    def _checkvolume(self, ssc):
        rms = ssc.rms()
        print("rms value = %f" % rms)
        # USBマイクが何Vの範囲を何分割してるのかわからないから電圧値変換係数がわからない
        # 20log_10(((rms / 電圧値変換係数) / 10^(マイク感度 / 20)) /基準音圧 )
        # rms_v = rms / ((2**16)/5)
        # pa = rms_v / (10**((self._DEVICE_SENS + self._AMP_GAIN) / 20))
        # db = 20 * np.log10(pa/self._STANDARD_PA)
        # print("rms_v:%f\tPa:%f\tDB:%f" % (rms_v, pa, db))
        # return db > self._DB_THRESHOLD
        return rms > self._RMS_THRESHOLD

    def _checkfrequency(self, ssc):
        fftwave, freq = ssc.ampspectrum()
        maxindex = np.nanargmax(fftwave)
        maxfreq = freq[maxindex]
        # self.plottest(freq, fftwave)
        print("freq value = %f" % maxfreq)
        if maxfreq > self._FREQ_LOWER and maxfreq < self._FREQ_UPPER:
            return True
        else:
            return False
    # cgiで使うのは禁止
    # def plottest(self, x, y):
        # plt.plot(x, y)
        # plt.show()
