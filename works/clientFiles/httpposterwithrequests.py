# coding:UTF-8
# http通信による文字POST,ファイルPOSTを行うクラス
# requestsモジュールを使用する

import mimetypes
import requests
from io import IOBase


class HTTPPosterWithRequests(object):
    """docstring for HTTPPoster"""
    # variables
    # private static const
    _MULTIPART_HEADERS = {
        "Content-Type": 'multipart/form-data; boundary=',
        "Accept-Language": "ja",
    }
    # private
    # str _url
    # dictionary _data

    # urlと辞書型のデータを受け取って初期化
    # //TODO 型チェックしてエラー吐くようにしたい
    def __init__(self, url, datadict, verify=None, cert=None):
        super(HTTPPosterWithRequests, self).__init__()
        self._url = url
        self._files, self._data = self._httpencode(datadict)
        self._verify = verify
        self._cert = cert

    # 辞書型データをエンコーディング fileが含まれていればmultipart_encode
    def _httpencode(self, datadict):
        havefile = False
        for key, data in datadict.items():
            if isinstance(data, IOBase):
                havefile = True
        if havefile:
            return self._multipart_encode(datadict)
        else:
            return self._normal_encode(datadict)

    # fileがある場合multipart_encodeのヘッダとbodyを作成してエンコード
    def _multipart_encode(self, datadict):
        files = {}
        others = {}
        for key, data in datadict.items():
            if isinstance(data, IOBase):
                files.update({key: data})
            else:
                others.update({key: data})
        return files, others

    # fileがなく文字列要素のみの場合urlencodeを利用してエンコード
    def _normal_encode(self, datadict):
        return None, datadict

    # urlにデータをPOSTし、帰って来たレスポンスを返り値とする
    def post(self):
        if self._files is None:
            if self._verify is None:
                response = requests.post(self._url, data=self._data)
            elif self._cert is None:
                response = requests.post(
                    self._url, data=self._data, verify=self._verify)
            else:
                response = requests.post(
                    self._url,
                    self._data,
                    verify=False,
                    cert=(
                        'client/certs/fixed-chu1.crt',
                        'client/private/fixed-chu1.key'))
        else:
            if self._verify is None:
                response = requests.post(
                    self._url, data=self._data, files=self._files)
            elif self._cert is None:
                response = requests.post(
                    self._url, data=self._data, files=self._files,
                    verify=self._verify)
            else:
                response = requests.post(
                    self._url,
                    files=self._files,
                    data=self._data,
                    verify=False,
                    cert=(
                        'client/certs/fixed-chu1.crt',
                        'client/private/fixed-chu1.key'))
        page = response.text
        print(page)
        return page

    # url変更 //TODO 型チェックとエラー吐くべき
    def seturl(self, url):
        self._url = url

    # 送信data変更 再エンコ
    def setdata(self, datadict):
        self._data = self._httpencode(datadict)


# 単体テスト
if __name__ == "__main__":
    # 現在時間取得
    import datetime
    d = datetime.datetime.today()
    datetime_str = d.strftime("%Y%m%d%H%M%S")[2:]
    with open("testwav.wav", "rb") as wavefile:
        data = {'name': 'insect_wave', 'id': 1, 'datetime1': datetime_str,
                'num': 1, 'file1': wavefile, 'temp1': 10.0, 'humid1': 11.0}
        url = "http://localhost:8000/cgi-bin/fixedchucgi.py"
        poster = HTTPPosterWithRequests(url, data)
        poster.post()
