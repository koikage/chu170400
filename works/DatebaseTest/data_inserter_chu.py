# coding:UTF-8
"""
localhostのchu_lingualデータベースにINSERTするマン
"""

import MySQLdb


# 構造体の代わり sqlになげるデータの型
class FixedChuData(object):
    def __init__(self, device_id, filepath, kind, datetime, temp, humid):
        super(FixedChuData, self).__init__()
        self.device_id = device_id
        self.filepath = filepath
        self.kind = kind
        self.datetime = datetime
        self.temp = temp
        self.humid = humid


# FixedChuData型のデータをデータベースに投げつける
class DataInserterChu(object):
    # variable
    # private const
    _USER_NAME = "chu_server"
    _PASSWORD = "Bicmos_4cmos"
    _HOST_NAME = "localhost"
    _DATABASE_NAME = "chu_lingualDB"

    def __init__(self):
        super(DataInserterChu, self).__init__()
        try:
            self._connection = MySQLdb.connect(
                user=self._USER_NAME,
                passwd=self._PASSWORD,
                host=self._HOST_NAME,
                db=self._DATABASE_NAME)
            self._cursor = self._connection.cursor()
        except:
            print("failed")

    # データは(sound_id,device_id,filepath,kind,datetime,temperature,humidity)のタプルでもらうものとする
    def insert(self, fixedChuData):
        device_id = fixedChuData.device_id
        filepath = fixedChuData.filepath
        kind = fixedChuData.kind
        datetime = fixedChuData.datetime
        temperature = fixedChuData.temp
        humidity = fixedChuData.humid
        sqlHead = ("INSERT INTO chu_lingual.insect_sound(device_id ,filepath ,"
                   "kind ,datetime ,temperature ,humidity)")
        sqlTail = "VALUES(%s,'%s','%s',%s,%s,%s)" % (
            device_id, filepath, kind, datetime, temperature, humidity)
        insertsql = sqlHead + sqlTail

        try:
            self._cursor.execute(insertsql)
            self._connection.commit()
        except Exception as e:
            print("false")
            print(e)
            print(sqlTail)


if __name__ == "__main__":
    chudatabaseinserter = DataInserterChu()
    testdata = FixedChuData(100, "tesstes", "teskin", "now()", 30, 80)
    chudatabaseinserter.insert(testdata)
