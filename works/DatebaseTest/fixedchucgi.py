#!/Users/ishiguro/.virtualenvs/env3/bin/python3
# coding: UTF-8

# macbookローカルで試すときはこっち
# !~/.virtualenvs/env3/bin/python3

# サーバで試すときにしてもvirtualenvしておく

import cgi
from data_inserter_chu import DataInserterChu
from data_inserter_chu import FixedChuData
from subprocess import Popen, PIPE
import sys


class FixedChuCgi(object):
    """docstring for FixedChuCgi"""
    _FORM_NAME = "insect_wave"
    _HTML_BODY = """
                <html><body>
                %s
                </body></html>"""

    def __init__(self):
        super(FixedChuCgi, self).__init__()

    # クラスのエントリーポイント的な
    def main(self):
        print('Content-type: text/html\n')
        form = cgi.FieldStorage()
        if 'name' in form:
            data = self.analysis_form(form)
            self.insert_to_database(data)
        else:
            print(self._HTML_BODY % 'CGI')

    # POSTされたFieldStorageのデータから必要なデータのリストを形成する
    def analysis_form(self, form):
        # POSTされたデータはform[タグ]の形でとれる
        name = form['name'].value
        if name != self._FORM_NAME:
            print(self._HTML_BODY % "name error")
            sys.exit()
        device_id = int(form['id'].value)
        data_num = int(form['num'].value)
        data_list = []
        for i in range(1, data_num+1):
            file_tag = "file" + str(i)
            temp_tag = "temp" + str(i)
            humid_tag = "humid" + str(i)
            datetime_tag = "datetime" + str(i)
            item_file = form[file_tag]
            filepath = ""
            if item_file.file:
                filepath = ('/Users/ishiguro/Desktop/Reseach/report/170400/'
                            'works/TestLocalServer/cgi-bin/getwavefiles/'
                            '%s' % item_file.filename)
                fout = open(filepath, 'wb')
                while True:
                    line = item_file.file.readline()
                    if not line:
                        break
                    fout.write(line)
                fout.close()
            else:
                print("不正なデータ構成")
                sys.exit()
                # errorだよ！
            kind = self.indentificate_mat(filepath)
            temp = form[temp_tag].value
            humid = form[humid_tag].value
            datetime = form[datetime_tag].value
            data = FixedChuData(
                device_id, filepath, kind, datetime, temp, humid)
            data_list.append(data)
        print("http ok")
        return data_list

    def indentificate_mat(self, path):
        # TODO ここをmatlabの識別器起動になおそうな
        # p = Popen(["matlab", なんやかんや])
        p = Popen(["echo", "tes"], stdout=PIPE)
        # stdoutを利用するwaitは出力が多いとデッドロックを起こすので注意
        if p.wait() != 0:
            print("exit code is not 0")
            return ""
        # 改行コード\nは抜く
        return p.stdout.read().decode('utf-8')[0:-1]

    def insert_to_database(self, data_list):
        data_inserter = DataInserterChu()
        for i in range(0, len(data_list)):
            data_inserter.insert(data_list[i])


if __name__ == "__main__":
    fxc = FixedChuCgi()
    fxc.main()
